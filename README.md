# S3 Bucket using CDK TypeScript

## Goals
* Set up an Amazon S3 bucket using the AWS Cloud Development Kit (CDK).
* Generate the CDK code with the help of CodeWhisperer.
* Configure the bucket to have features such as versioning enabled and to use encryption.

## Steps
1. In CodeCatalyst, create a new project.
2. Set up a new empty development environment in Cloud9.
3. In the AWS Management Console, add a new IAM user with permissions attched with policies 
    * `AmazonS3FullAccess`
    * `AWSCloudFormationFullAccess`
    * `IAMFullAccess`
    * `AWSCloud9User`
    * `AWSCodeBuildDeveloperAccess`
    * `AWSLambda_FullAccess`
4. Complete the IAM user setup and go to the "Security Credentials" section to generate an access key.
5. Return to the Cloud9 environment and input `aws configure` in the terminal.
6. When prompted, enter the previously stored AWS credentials and retain the default settings for the region and output format.
7. In the terminal, create a new directory for your project and navigate into it.
8. Initialize a new CDK project in TypeScript using `cdk init app --language typescript`.
9. With the CDK template ready, incorporate the CodeWhisperer-generated code into the `lib` and `bin` directories.
10. Compile the project with `npm run build`.
11. Produce a CloudFormation template using `cdk synth`.
12. Deploy the generated CloudFormation template with `cdk deploy`.
13. If deployed failed, add the reported role to the `IAM roles`.

## S3 Bucket Screenshot
![s3](https://gitlab.com/dukeaiml/IDS721/zhankai_ye_mini_project_3/-/raw/master/uploads/s3_2.png)
* Versioning enabled
![version](https://gitlab.com/dukeaiml/IDS721/zhankai_ye_mini_project_3/-/raw/master/uploads/s3_versioning.png)
* Encryption enabled
![Encryption](https://gitlab.com/dukeaiml/IDS721/zhankai_ye_mini_project_3/-/raw/master/uploads/s3_Encryption.png)

## AWS Codewhisperer
To create code for an S3 bucket, I utilized AWS CodeWhisperer with these prompts:
* Within lib/mini_proj3-stack.ts, I prompted it with `// make an S3 bucket` and `// make an S3 bucket that enables versioning and encryption` to produce the code needed for the S3 bucket.
* For bin/mini_proj3.ts, I prompted `// add necessary variables to create the S3 bucket` to generate the required variables."
